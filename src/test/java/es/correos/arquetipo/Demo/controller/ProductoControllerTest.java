package es.correos.arquetipo.Demo.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import es.correos.arquetipo.Demo.dto.ProductoDTO;
import es.correos.arquetipo.Demo.service.IProductoService;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductoRestController.class)
public class ProductoControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private IProductoService productoService;
	
	protected static ObjectMapper om = new ObjectMapper();
	
	//Capta valores Json (formato con comillas)
	@Before
	public void configuracionMapper() {
		om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		JacksonTester.initFields(this, om);
	}
	
	@Test
	public void saveProductoTest() {
		ProductoDTO productoRequest = new ProductoDTO();
		
		productoRequest.setCategoriaId(1);
		productoRequest.setDescripcion("chocolate");
		productoRequest.setExistencia(100);
		productoRequest.setPrecioUnit(3.5);
		productoRequest.setProveedorId(3);
		
		
	}
}