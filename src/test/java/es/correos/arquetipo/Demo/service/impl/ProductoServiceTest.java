package es.correos.arquetipo.Demo.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import es.correos.arquetipo.Demo.domain.Producto;
import es.correos.arquetipo.Demo.dto.ProductoDTO;
import es.correos.arquetipo.Demo.mapper.ProductoMapper;
import es.correos.arquetipo.Demo.repository.IProductoRepository;

@RunWith(SpringRunner.class)
public class ProductoServiceTest {
	
	/****************** Service  **********************/
	@Mock 
	private IProductoRepository productoRepository;
	
	@Mock
	private ProductoMapper productoMapper;
	
	@InjectMocks
	private ProductoServiceImpl productoService;
	/************************************************/
	
	@Test
	public void saveTest(){
		ProductoDTO productoDtoRequest = new ProductoDTO();
		
		productoDtoRequest.setCategoriaId(1);
		productoDtoRequest.setDescripcion("pro");
		productoDtoRequest.setExistencia(1);
		productoDtoRequest.setPrecioUnit(1.5);
		productoDtoRequest.setProveedorId(1);
		
		productoService.save(productoDtoRequest);
	}
	
	@Test
	public void listAllTest(){
		
		List<Producto> list = Arrays.asList();
		
		when(productoMapper.productoDtoToProducto(Mockito.anyList())).thenReturn(list);
		when(productoRepository.findAll()).thenReturn(list);
		
		System.out.println("result: " + productoService.listAll().toString());
		System.out.println("result: " + list.toString());
		//assert(result.isEmpty());
		//assertNotNull(productoListDtoRequest);
	}
}