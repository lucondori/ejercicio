package es.correos.arquetipo.Demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.correos.arquetipo.Demo.dto.OrdenDetalleDTO;
import es.correos.arquetipo.Demo.mapper.OrdenDetalleMapper;
import es.correos.arquetipo.Demo.repository.IOrdenDetalleRepository;
import es.correos.arquetipo.Demo.service.IOrdenDetalleService;

@Service 
@Transactional
public class OrdenDetalleServiceImpl implements IOrdenDetalleService {
	
	@Autowired
	private IOrdenDetalleRepository ordenDetalleRepository;
	
	@Override
	public List<OrdenDetalleDTO> listAll() {
		return OrdenDetalleMapper.INSTANCE.OrdenDetalleToOrdenDetalleDto(ordenDetalleRepository.findAll());
	}
	
	@Override
	public void delete(Integer ordenId, Integer detalleId) {
		
	}
}