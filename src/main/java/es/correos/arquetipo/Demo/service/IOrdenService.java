package es.correos.arquetipo.Demo.service;

import java.util.List;

import es.correos.arquetipo.Demo.dto.OrdenDTO;

public interface IOrdenService {
	
	void save(OrdenDTO ordenDto);
	
	OrdenDTO saveMaster(OrdenDTO ordenDto);
	
	List<OrdenDTO> listAll();
	
	void update(OrdenDTO ordenDto);
	
	void delete(Integer ordenId);
	
	OrdenDTO findById(Integer ordenId);
}