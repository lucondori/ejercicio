package es.correos.arquetipo.Demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.correos.arquetipo.Demo.dto.OrdenDTO;
import es.correos.arquetipo.Demo.dto.OrdenDetalleDTO;
import es.correos.arquetipo.Demo.mapper.OrdenDetalleMapper;
import es.correos.arquetipo.Demo.mapper.OrdenMapper;
import es.correos.arquetipo.Demo.repository.IOrdenDetalleRepository;
import es.correos.arquetipo.Demo.repository.IOrdenRepository;
import es.correos.arquetipo.Demo.service.IOrdenService;

@Service
@Transactional
public class OrdenServiceImpl implements IOrdenService {

	@Autowired
	private IOrdenRepository ordenRepository;
	
	@Autowired
	private IOrdenDetalleRepository ordenDetalleRepository;
	
	@Override
	public OrdenDTO saveMaster(OrdenDTO ordenDto) {
		return OrdenMapper.INSTANCE.ordenToOrdenDto(
			ordenRepository.save(OrdenMapper.INSTANCE.ordenDtoToOrden(ordenDto))
		);
	}
	
	@Override // VALIDAR -> throws
	public void save(OrdenDTO ordenDto) {
		OrdenDTO ordenCreated =  saveMaster(ordenDto);

		System.out.println(ordenCreated.toString());
		
		if(saveMaster(ordenDto) != null ) {
			for(OrdenDetalleDTO o : ordenDto.getOrdenDetalleList()) {
				//o.
				//o.setOrdenDetalleId(ordenDetalleId);
				System.out.println(o.toString());
			}
		}
		/*
		ordenDetalleRepository.saveAll( 
			OrdenDetalleMapper.INSTANCE.OrdenDetalleDtoToOrdenDetalle(
				ordenDto.getOrdenDetalleList()
			)
		);*/
	}
	
	@Override
	public List<OrdenDTO> listAll() {
		return OrdenMapper.INSTANCE.ordenToOrdenDto(ordenRepository.findAll());
	}

	@Override
	public void update(OrdenDTO ordenDto) {
		ordenRepository.saveAndFlush(OrdenMapper.INSTANCE.ordenDtoToOrden(ordenDto));
	}

	@Override
	public void delete(Integer ordenId) {
		ordenDetalleRepository.deleteAll(
			ordenDetalleRepository.findByOrden(ordenRepository.getOne(ordenId))
		);
		ordenRepository.deleteById(ordenId);
	}
	
	@Override
	public OrdenDTO findById(Integer ordenId) {
		return OrdenMapper.INSTANCE.ordenToOrdenDto(ordenRepository.getOne(ordenId));
	}
}