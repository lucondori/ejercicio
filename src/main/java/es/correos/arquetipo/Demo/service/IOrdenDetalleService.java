package es.correos.arquetipo.Demo.service;

import java.util.List;

import es.correos.arquetipo.Demo.dto.OrdenDetalleDTO;

public interface IOrdenDetalleService {
	
	List<OrdenDetalleDTO> listAll();
	
	void delete(Integer ordenId, Integer detalleId);
	
	//List<OrdenDetalleDTO> findAllByOrdenId(Integer ordenId);
}