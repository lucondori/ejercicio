package es.correos.arquetipo.Demo.service;

import java.util.List;

import es.correos.arquetipo.Demo.dto.ProductoDTO;

public interface IProductoService {
	
	ProductoDTO save(ProductoDTO productoDto);
	
	List<ProductoDTO> listAll();
	
	void update(ProductoDTO productoDto);
	
	void delete(Integer productoId);
	
	ProductoDTO findById(Integer productoId);
}