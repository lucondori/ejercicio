package es.correos.arquetipo.Demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.correos.arquetipo.Demo.domain.Producto;
import es.correos.arquetipo.Demo.dto.ProductoDTO;
import es.correos.arquetipo.Demo.mapper.ProductoMapper;
import es.correos.arquetipo.Demo.repository.IProductoRepository;
import es.correos.arquetipo.Demo.service.IProductoService;

@Service
@Transactional 
public class ProductoServiceImpl implements IProductoService {
	
	@Autowired
	private ProductoMapper productoMapper;
	
	@Autowired
	private IProductoRepository productoRepository;
	
	@Override // VALIDAR -> throws
	public ProductoDTO save(ProductoDTO productoDtoRequest) {
		
		Producto productoRequest = productoMapper.productoDtoToProducto(productoDtoRequest);
		ProductoDTO productoDtoResponse = productoMapper.productoToProductoDto(
				productoRepository.save(productoRequest)
		);
		return productoDtoResponse;
	}
	
	@Override
	public List<ProductoDTO> listAll() {
		return productoMapper.productoToProductoDto(productoRepository.findAll());
	}
	
	@Override
	public void update(ProductoDTO productoDto) {
		productoRepository.saveAndFlush(productoMapper.productoDtoToProducto(productoDto));
	}
	
	@Override
	public void delete(Integer productoId) {
		productoRepository.deleteById(productoId);
	}
	
	@Override
	public ProductoDTO findById(Integer productoId) {
		return productoMapper.productoToProductoDto(productoRepository.getOne(productoId));
	}
}