package es.correos.arquetipo.Demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.correos.arquetipo.Demo.domain.Orden;

public interface IOrdenRepository extends JpaRepository<Orden, Integer> {

}