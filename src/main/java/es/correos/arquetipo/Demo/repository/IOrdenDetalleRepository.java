package es.correos.arquetipo.Demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import es.correos.arquetipo.Demo.domain.Orden;
import es.correos.arquetipo.Demo.domain.OrdenDetalle;
import es.correos.arquetipo.Demo.domain.OrdenDetalleId;

public interface IOrdenDetalleRepository extends JpaRepository<OrdenDetalle, OrdenDetalleId> {
	
	List<OrdenDetalle> findByOrden(Orden orden);
	
	//https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
}