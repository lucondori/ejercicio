package es.correos.arquetipo.Demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.correos.arquetipo.Demo.domain.Producto;

public interface IProductoRepository extends JpaRepository<Producto, Integer>{

}