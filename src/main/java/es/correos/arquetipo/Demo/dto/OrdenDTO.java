package es.correos.arquetipo.Demo.dto;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class OrdenDTO {

	private Integer ordenId;
	
	private Integer empleadoId;
	
	private Integer clienteId;
	
	private Date fechaOrden;
	
	private Integer descuento;
	
	private List<OrdenDetalleDTO> ordenDetalleList;
}