package es.correos.arquetipo.Demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductoDTO {
	
	private Integer productoId;
	
	private Integer proveedorId;
	
	private Integer categoriaId;
	
	private String descripcion;
	
	private Double precioUnit;
	
	private Integer existencia;
}