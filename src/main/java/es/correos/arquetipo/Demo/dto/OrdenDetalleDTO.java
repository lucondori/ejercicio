package es.correos.arquetipo.Demo.dto;

import javax.persistence.EmbeddedId;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import es.correos.arquetipo.Demo.domain.Orden;
import es.correos.arquetipo.Demo.domain.OrdenDetalleId;
import es.correos.arquetipo.Demo.domain.Producto;
import lombok.Data;

@Data
@JsonIgnoreProperties({"orden"})
public class OrdenDetalleDTO {
	
	@EmbeddedId
	private OrdenDetalleId ordenDetalleId;
	
	@MapsId("productoId")
	@ManyToOne
	private Producto producto;
	
	@MapsId("ordenId")
	@ManyToOne(targetEntity = Orden.class)
	private Orden orden;
	
	private Integer productoCantidad;
	
}