package es.correos.arquetipo.Demo.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "detalle_ordenes")
@JsonIgnoreProperties({"orden"})
public class OrdenDetalle {
	
	@EmbeddedId
	private OrdenDetalleId ordenDetalleId;
	
	@JoinColumn(name ="ordenid", insertable = false, updatable = false)
	@MapsId("ordenId")
	@ManyToOne
	private Orden orden;
	
	@JoinColumn(name ="productoid", insertable = false, updatable = false)
	@MapsId("productoId")
	@ManyToOne
	private Producto producto;
	
	@Column(name = "cantidad")
	private Integer productoCantidad;
	
	public OrdenDetalle(Integer ordenId, Integer detalleId) {
		ordenDetalleId = new OrdenDetalleId(ordenId, detalleId);
    }
}