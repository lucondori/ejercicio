package es.correos.arquetipo.Demo.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ordenes")
public class Orden {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orden_seq")
	@SequenceGenerator(name="orden_seq", sequenceName = "orden_seq", allocationSize=1)
	@Column(name = "ordenid")
	private Integer ordenId;
	
	@Column(name = "empleadoid")
	private Integer empleadoId;
	
	@Column(name = "clienteid")
	private Integer clienteId;
	
	@Column(name = "fechaorden", columnDefinition = "date")
	private Date fechaOrden;
	
	@Column(name = "descuento")
	private Integer descuento;
	
	@OneToMany(mappedBy = "orden")
	private List<OrdenDetalle> ordenDetalleList;
	
}