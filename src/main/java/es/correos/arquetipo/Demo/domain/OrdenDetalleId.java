package es.correos.arquetipo.Demo.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class OrdenDetalleId implements Serializable{
	
	@Column(name = "ordenid")
	private Integer ordenId;
	
	@Column(name = "detalleid")
	private Integer detalleId;
}