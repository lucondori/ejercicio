package es.correos.arquetipo.Demo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "productos")
public class Producto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "producto_seq")
	@SequenceGenerator(name="producto_seq", sequenceName = "producto_seq", allocationSize=1)
	@Column(name = "productoid")
	private Integer productoId;
	
	@Column(name = "proveedorid")
	private Integer proveedorId;
	
	@Column(name = "categoriaid")
	private Integer categoriaId;
	
	@Column(name = "descripcion", columnDefinition = "bpchar")
	private String descripcion;
	
	@Column(name = "preciounit", columnDefinition = "numeric")
	private Double precioUnit;
	
	@Column(name = "existencia")
	private Integer existencia;
}