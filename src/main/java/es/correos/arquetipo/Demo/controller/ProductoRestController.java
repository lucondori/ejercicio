package es.correos.arquetipo.Demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.correos.arquetipo.Demo.dto.ProductoDTO;
import es.correos.arquetipo.Demo.service.IProductoService;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("/product")
public class ProductoRestController {
	
	@Autowired
	private IProductoService productoService;
	
	/*Create*/
	@PostMapping(value = "/save")
	@ApiResponse(code = 401, message = "No se encontro el resultado")
	public ResponseEntity<ProductoDTO> saveProduct(@RequestBody ProductoDTO productoDto){
		ProductoDTO productoDtoResponse = productoService.save(productoDto);
		return new ResponseEntity<>(productoDtoResponse, HttpStatus.CREATED);
	}
	
	/*Read*/
	@GetMapping(value = "/list", headers = "Accept=application/json")
	@ApiResponse(code = 401, message = "No se encontro el resultado")
	public ResponseEntity<List<ProductoDTO>> listAllProducts(){
		List<ProductoDTO> productDtoList = productoService.listAll();
		if (productDtoList.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(productDtoList, HttpStatus.OK);
	}
	
	/*Update*/
	@PutMapping(value = "/{id}")
	@ApiResponse(code = 401, message = "No se encontro el resultado")
	public ResponseEntity<ProductoDTO> updateProduct(@PathVariable("id") Integer productoId, @RequestBody ProductoDTO productoDto){
		ProductoDTO productFound = productoService.findById(productoId);
		
		productFound.setCategoriaId(productoDto.getCategoriaId());
		productFound.setDescripcion(productoDto.getDescripcion());
		productFound.setExistencia(productoDto.getExistencia());
		productFound.setPrecioUnit(productoDto.getPrecioUnit());
		productFound.setProveedorId(productoDto.getProveedorId());
		//productFound.setProductoId(productoDto.getProductoId());
		
		productoService.update(productFound);
		
		return new ResponseEntity<>(productFound,HttpStatus.OK);
	}
	
	/*Delete*/
	@DeleteMapping(value = "/{id}")
	@ApiResponse(code = 401, message = "No autorizado")
	public ResponseEntity<ProductoDTO> deleteProduct(@PathVariable("id") Integer productoId){
		ProductoDTO productFound = productoService.findById(productoId);
		
		productoService.delete(productoId);
		
		return new ResponseEntity<>(productFound, HttpStatus.ACCEPTED);
	}
}