package es.correos.arquetipo.Demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.correos.arquetipo.Demo.dto.OrdenDetalleDTO;
import es.correos.arquetipo.Demo.service.IOrdenDetalleService;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("/orderDetail")
public class OrdenDetalleRestController {
	
	@Autowired
	private IOrdenDetalleService ordenDetalleService;
	
	/*Read*/
	@GetMapping(value = "/list", headers = "Accept=application/json")
	@ApiResponse(code = 401, message = "No se encontro el resultado")
	public ResponseEntity<List<OrdenDetalleDTO>> listAllOrdersDetails(){
		List<OrdenDetalleDTO> orderDetailList = ordenDetalleService.listAll();
		if(orderDetailList.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(orderDetailList, HttpStatus.OK);
	}
	
}