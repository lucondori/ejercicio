package es.correos.arquetipo.Demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.correos.arquetipo.Demo.dto.OrdenDTO;
import es.correos.arquetipo.Demo.service.IOrdenService;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("/order")
public class OrdenRestController {
	
	@Autowired
	private IOrdenService ordenService;
	
	/*Create*/
	@PostMapping(value = "/save")
	@ApiResponse(code = 401, message = "No se encontro el resultado")
	public ResponseEntity<OrdenDTO> saveOrden(@RequestBody OrdenDTO ordenDto){
		ordenService.save(ordenDto);
		return new ResponseEntity<>(ordenDto, HttpStatus.CREATED);
	}
	
	
	/*Read*/
	@GetMapping(value = "/list", headers = "Accept=application/json")
	@ApiResponse(code = 401, message = "No se encontro el resultado")
	public ResponseEntity<List<OrdenDTO>> listAllOrders(){
		List<OrdenDTO> orderList = ordenService.listAll();
		if(orderList.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(orderList, HttpStatus.OK);
	}
	
	/*Delete*/
	@DeleteMapping(value = "/{ordenId}")
	@ApiResponse(code = 401, message = "No autorizado")
	public ResponseEntity<OrdenDTO> deleteOrder(@PathVariable("ordenId") Integer ordenId){
		OrdenDTO orderFound = ordenService.findById(ordenId);
		ordenService.delete(ordenId);
		return new ResponseEntity<>(orderFound, HttpStatus.ACCEPTED);
	}
}