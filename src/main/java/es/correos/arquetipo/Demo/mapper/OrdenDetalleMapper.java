package es.correos.arquetipo.Demo.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import es.correos.arquetipo.Demo.domain.OrdenDetalle;
import es.correos.arquetipo.Demo.dto.OrdenDetalleDTO;

@Mapper
public interface OrdenDetalleMapper {
	
	OrdenDetalleMapper INSTANCE = Mappers.getMapper(OrdenDetalleMapper.class);
	
	//@Mapping(target = "orden", ignore = true)
	@IterableMapping(elementTargetType = OrdenDetalleDTO.class)
	List<OrdenDetalleDTO> OrdenDetalleToOrdenDetalleDto(List<OrdenDetalle> ordenDetalleList); 
	
	OrdenDetalleDTO OrdenDetalleToOrdenDetalleDto(OrdenDetalle ordenDetalle);
	
	//@Mapping(target = "orden", ignore = true)
	List<OrdenDetalle> OrdenDetalleDtoToOrdenDetalle(List<OrdenDetalleDTO> ordenDetalleDtoList);
	
}