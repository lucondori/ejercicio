package es.correos.arquetipo.Demo.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import es.correos.arquetipo.Demo.domain.Orden;
import es.correos.arquetipo.Demo.dto.OrdenDTO;

@Mapper
public interface OrdenMapper {
	
	OrdenMapper INSTANCE = Mappers.getMapper(OrdenMapper.class);
	
	Orden ordenDtoToOrden(OrdenDTO ordenDto);
	
	@IterableMapping(elementTargetType = OrdenDTO.class)
	List<OrdenDTO> ordenToOrdenDto(List<Orden> ordenList);
	
	OrdenDTO ordenToOrdenDto(Orden orden);
}