package es.correos.arquetipo.Demo.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
//import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapping;

import es.correos.arquetipo.Demo.domain.Producto;
import es.correos.arquetipo.Demo.dto.ProductoDTO;

@Mapper
public interface ProductoMapper {
	
	//ProductoMapper INSTANCE = Mappers.getMapper(ProductoMapper.class);
	//@Mapping()
	Producto productoDtoToProducto(ProductoDTO productoDto);
	
	@IterableMapping(elementTargetType = ProductoDTO.class)
	List<ProductoDTO> productoToProductoDto(List<Producto> productoList);
	
	@IterableMapping(elementTargetType = Producto.class)
	List<Producto> productoDtoToProducto(List<ProductoDTO> productoDtoList);
	
	ProductoDTO productoToProductoDto(Producto producto);
}