package es.correos.arquetipo.Demo.mapper;

import es.correos.arquetipo.Demo.domain.OrdenDetalle;
import es.correos.arquetipo.Demo.dto.OrdenDetalleDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.6 (Oracle Corporation)"
)
@Component
public class OrdenDetalleMapperImpl implements OrdenDetalleMapper {

    @Override
    public List<OrdenDetalleDTO> OrdenDetalleToOrdenDetalleDto(List<OrdenDetalle> ordenDetalleList) {
        if ( ordenDetalleList == null ) {
            return null;
        }

        List<OrdenDetalleDTO> list = new ArrayList<OrdenDetalleDTO>( ordenDetalleList.size() );
        for ( OrdenDetalle ordenDetalle : ordenDetalleList ) {
            list.add( OrdenDetalleToOrdenDetalleDto( ordenDetalle ) );
        }

        return list;
    }

    @Override
    public OrdenDetalleDTO OrdenDetalleToOrdenDetalleDto(OrdenDetalle ordenDetalle) {
        if ( ordenDetalle == null ) {
            return null;
        }

        OrdenDetalleDTO ordenDetalleDTO = new OrdenDetalleDTO();

        ordenDetalleDTO.setOrdenDetalleId( ordenDetalle.getOrdenDetalleId() );
        ordenDetalleDTO.setProducto( ordenDetalle.getProducto() );
        ordenDetalleDTO.setOrden( ordenDetalle.getOrden() );
        ordenDetalleDTO.setProductoCantidad( ordenDetalle.getProductoCantidad() );

        return ordenDetalleDTO;
    }

    @Override
    public List<OrdenDetalle> OrdenDetalleDtoToOrdenDetalle(List<OrdenDetalleDTO> ordenDetalleDtoList) {
        if ( ordenDetalleDtoList == null ) {
            return null;
        }

        List<OrdenDetalle> list = new ArrayList<OrdenDetalle>( ordenDetalleDtoList.size() );
        for ( OrdenDetalleDTO ordenDetalleDTO : ordenDetalleDtoList ) {
            list.add( ordenDetalleDTOToOrdenDetalle( ordenDetalleDTO ) );
        }

        return list;
    }

    protected OrdenDetalle ordenDetalleDTOToOrdenDetalle(OrdenDetalleDTO ordenDetalleDTO) {
        if ( ordenDetalleDTO == null ) {
            return null;
        }

        OrdenDetalle ordenDetalle = new OrdenDetalle();

        ordenDetalle.setOrdenDetalleId( ordenDetalleDTO.getOrdenDetalleId() );
        ordenDetalle.setOrden( ordenDetalleDTO.getOrden() );
        ordenDetalle.setProducto( ordenDetalleDTO.getProducto() );
        ordenDetalle.setProductoCantidad( ordenDetalleDTO.getProductoCantidad() );

        return ordenDetalle;
    }
}
