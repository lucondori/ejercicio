package es.correos.arquetipo.Demo.mapper;

import es.correos.arquetipo.Demo.domain.Orden;
import es.correos.arquetipo.Demo.domain.OrdenDetalle;
import es.correos.arquetipo.Demo.dto.OrdenDTO;
import es.correos.arquetipo.Demo.dto.OrdenDetalleDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.6 (Oracle Corporation)"
)
@Component
public class OrdenMapperImpl implements OrdenMapper {

    @Override
    public Orden ordenDtoToOrden(OrdenDTO ordenDto) {
        if ( ordenDto == null ) {
            return null;
        }

        Orden orden = new Orden();

        orden.setOrdenId( ordenDto.getOrdenId() );
        orden.setEmpleadoId( ordenDto.getEmpleadoId() );
        orden.setClienteId( ordenDto.getClienteId() );
        orden.setFechaOrden( ordenDto.getFechaOrden() );
        orden.setDescuento( ordenDto.getDescuento() );
        orden.setOrdenDetalleList( ordenDetalleDTOListToOrdenDetalleList( ordenDto.getOrdenDetalleList() ) );

        return orden;
    }

    @Override
    public List<OrdenDTO> ordenToOrdenDto(List<Orden> ordenList) {
        if ( ordenList == null ) {
            return null;
        }

        List<OrdenDTO> list = new ArrayList<OrdenDTO>( ordenList.size() );
        for ( Orden orden : ordenList ) {
            list.add( ordenToOrdenDto( orden ) );
        }

        return list;
    }

    @Override
    public OrdenDTO ordenToOrdenDto(Orden orden) {
        if ( orden == null ) {
            return null;
        }

        OrdenDTO ordenDTO = new OrdenDTO();

        ordenDTO.setOrdenId( orden.getOrdenId() );
        ordenDTO.setEmpleadoId( orden.getEmpleadoId() );
        ordenDTO.setClienteId( orden.getClienteId() );
        ordenDTO.setFechaOrden( orden.getFechaOrden() );
        ordenDTO.setDescuento( orden.getDescuento() );
        ordenDTO.setOrdenDetalleList( ordenDetalleListToOrdenDetalleDTOList( orden.getOrdenDetalleList() ) );

        return ordenDTO;
    }

    protected OrdenDetalle ordenDetalleDTOToOrdenDetalle(OrdenDetalleDTO ordenDetalleDTO) {
        if ( ordenDetalleDTO == null ) {
            return null;
        }

        OrdenDetalle ordenDetalle = new OrdenDetalle();

        ordenDetalle.setOrdenDetalleId( ordenDetalleDTO.getOrdenDetalleId() );
        ordenDetalle.setOrden( ordenDetalleDTO.getOrden() );
        ordenDetalle.setProducto( ordenDetalleDTO.getProducto() );
        ordenDetalle.setProductoCantidad( ordenDetalleDTO.getProductoCantidad() );

        return ordenDetalle;
    }

    protected List<OrdenDetalle> ordenDetalleDTOListToOrdenDetalleList(List<OrdenDetalleDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<OrdenDetalle> list1 = new ArrayList<OrdenDetalle>( list.size() );
        for ( OrdenDetalleDTO ordenDetalleDTO : list ) {
            list1.add( ordenDetalleDTOToOrdenDetalle( ordenDetalleDTO ) );
        }

        return list1;
    }

    protected OrdenDetalleDTO ordenDetalleToOrdenDetalleDTO(OrdenDetalle ordenDetalle) {
        if ( ordenDetalle == null ) {
            return null;
        }

        OrdenDetalleDTO ordenDetalleDTO = new OrdenDetalleDTO();

        ordenDetalleDTO.setOrdenDetalleId( ordenDetalle.getOrdenDetalleId() );
        ordenDetalleDTO.setProducto( ordenDetalle.getProducto() );
        ordenDetalleDTO.setOrden( ordenDetalle.getOrden() );
        ordenDetalleDTO.setProductoCantidad( ordenDetalle.getProductoCantidad() );

        return ordenDetalleDTO;
    }

    protected List<OrdenDetalleDTO> ordenDetalleListToOrdenDetalleDTOList(List<OrdenDetalle> list) {
        if ( list == null ) {
            return null;
        }

        List<OrdenDetalleDTO> list1 = new ArrayList<OrdenDetalleDTO>( list.size() );
        for ( OrdenDetalle ordenDetalle : list ) {
            list1.add( ordenDetalleToOrdenDetalleDTO( ordenDetalle ) );
        }

        return list1;
    }
}
