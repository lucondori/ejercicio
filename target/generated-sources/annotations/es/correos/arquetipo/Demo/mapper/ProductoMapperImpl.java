package es.correos.arquetipo.Demo.mapper;

import es.correos.arquetipo.Demo.domain.Producto;
import es.correos.arquetipo.Demo.dto.ProductoDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.6 (Oracle Corporation)"
)
@Component
public class ProductoMapperImpl implements ProductoMapper {

    @Override
    public Producto productoDtoToProducto(ProductoDTO productoDto) {
        if ( productoDto == null ) {
            return null;
        }

        Producto producto = new Producto();

        producto.setProductoId( productoDto.getProductoId() );
        producto.setProveedorId( productoDto.getProveedorId() );
        producto.setCategoriaId( productoDto.getCategoriaId() );
        producto.setDescripcion( productoDto.getDescripcion() );
        producto.setPrecioUnit( productoDto.getPrecioUnit() );
        producto.setExistencia( productoDto.getExistencia() );

        return producto;
    }

    @Override
    public List<ProductoDTO> productoToProductoDto(List<Producto> productoList) {
        if ( productoList == null ) {
            return null;
        }

        List<ProductoDTO> list = new ArrayList<ProductoDTO>( productoList.size() );
        for ( Producto producto : productoList ) {
            list.add( productoToProductoDto( producto ) );
        }

        return list;
    }

    @Override
    public List<Producto> productoDtoToProducto(List<ProductoDTO> productoDtoList) {
        if ( productoDtoList == null ) {
            return null;
        }

        List<Producto> list = new ArrayList<Producto>( productoDtoList.size() );
        for ( ProductoDTO productoDTO : productoDtoList ) {
            list.add( productoDtoToProducto( productoDTO ) );
        }

        return list;
    }

    @Override
    public ProductoDTO productoToProductoDto(Producto producto) {
        if ( producto == null ) {
            return null;
        }

        ProductoDTO productoDTO = new ProductoDTO();

        productoDTO.setProductoId( producto.getProductoId() );
        productoDTO.setProveedorId( producto.getProveedorId() );
        productoDTO.setCategoriaId( producto.getCategoriaId() );
        productoDTO.setDescripcion( producto.getDescripcion() );
        productoDTO.setPrecioUnit( producto.getPrecioUnit() );
        productoDTO.setExistencia( producto.getExistencia() );

        return productoDTO;
    }
}
